package com.practice.breakingbad.data.db

import com.practice.breakingbad.internal.EntityMapper
import com.practice.breakingbad.model.ActorModel

class ActorEntityMapper():EntityMapper<ActorEntity, ActorModel> {
    override fun toModelObject(actorEntity: ActorEntity): ActorModel {
        return ActorModel(id=actorEntity.id,
            appearance = actorEntity.appearance,
            img = actorEntity.img,
            name = actorEntity.name,
            nickname = actorEntity.nickname,
            occupation = actorEntity.occupation,
            status = actorEntity.status

        )
    }

    override fun toModelListObject(actorEntityList: List<ActorEntity>): List<ActorModel> {
        return actorEntityList.map {
            toModelObject(it)
        }
    }
}