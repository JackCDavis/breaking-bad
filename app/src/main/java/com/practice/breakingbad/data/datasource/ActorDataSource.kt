package com.practice.breakingbad.data.datasource

import androidx.lifecycle.LiveData
import com.practice.breakingbad.data.network.ActorNetwork

interface ActorDataSource {

    val actorList:LiveData<List<ActorNetwork>>

    suspend fun fetchActorList()
}