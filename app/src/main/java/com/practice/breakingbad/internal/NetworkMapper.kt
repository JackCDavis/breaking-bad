package com.practice.breakingbad.internal

interface NetworkMapper<NetworkObj, EntityObj> {
    fun toEntityObj(networkObj: NetworkObj):EntityObj
    fun toEntityListObj(networkObjList: List<NetworkObj>):List<EntityObj>
}