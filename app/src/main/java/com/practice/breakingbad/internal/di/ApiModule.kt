package com.practice.breakingbad.internal.di

import com.practice.breakingbad.data.network.ActorApiService
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single {
        get<Retrofit>().create(ActorApiService::class.java)
    }
}