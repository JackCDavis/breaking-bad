package com.practice.breakingbad.internal

import com.practice.breakingbad.data.db.ActorEntity

interface EntityMapper<EntityObject, ModelObject> {
    fun toModelObject(entityObject: EntityObject): ModelObject
    fun toModelListObject(actorEntityList: List<ActorEntity>): List<ModelObject>
}