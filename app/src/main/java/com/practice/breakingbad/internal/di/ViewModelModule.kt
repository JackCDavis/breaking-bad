package com.practice.breakingbad.internal.di

import com.practice.breakingbad.ui.actorlist.ActorListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewmodelModule = module {
    viewModel { ActorListViewModel(get()) }
}