package com.practice.breakingbad.internal.di

import com.practice.breakingbad.data.repository.ActorRepository
import com.practice.breakingbad.data.repository.ActorRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single {
        ActorRepositoryImpl(get(), get()) as ActorRepository
    }
}