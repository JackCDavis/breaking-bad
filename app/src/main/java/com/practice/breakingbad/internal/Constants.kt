package com.practice.breakingbad.internal

object Constants {
    const val BASE_URL = "https://breakingbadapi.com/api/"
    const val URL_LINK = "characters"
}