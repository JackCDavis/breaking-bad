package com.practice.breakingbad.ui.actordetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.practice.breakingbad.R
import com.practice.breakingbad.databinding.FragmentActorDetailBinding


/**
 * A simple [Fragment] subclass.
 * Use the [ActorDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ActorDetailFragment : Fragment() {
    private lateinit var binding: FragmentActorDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_actor_detail, container, false)
        var actor = ActorDetailFragmentArgs.fromBundle(requireArguments()).actorModel
        binding.results = actor
        return binding.root
    }
}