package com.practice.breakingbad.data.db

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import com.google.common.truth.Truth.assertThat
import com.practice.breakingbad.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.*

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class MyNewsDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: ActorDatabase
    private lateinit var dao: ActorDao


    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(), ActorDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.actorDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertActors() = runBlocking {
        withContext(Dispatchers.Main) {
            val actorEntity = ActorEntity(1, listOf("1"), "testimg","name", "nickname", listOf("occupation"), "status")
            val actorEntity2 = ActorEntity(2, listOf("2"), "testimg2","name2", "nickname2", listOf("occupation2"), "status2")
            val actorList = mutableListOf<ActorEntity>(actorEntity, actorEntity2)
            dao.addActors(actorList)

            val actorDbList = dao.getActors().getOrAwaitValue()
            Log.i("TAG", "insertActors: ${actorDbList.get(1).id}")
            //Assert.assertTrue(ReflectionEquals(actorEntity).equals(actorDbList[1]))
            //assertThat(actorDbList).contains(actorEntity)
            //assertThat(actorDbList).contains(actorEntity2)

            val actorAppearanceList = dao.getActorsBySeason("%1%").getOrAwaitValue()
            assertThat(actorAppearanceList).hasSize(1)
        }

    }
}