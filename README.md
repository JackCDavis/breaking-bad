This is a read me for the breaking bad character app. 

Project summary:

The project displays all the characters from the show Breaking Bad. You can then click on the character to display more information on the character such as alias and the series (season) they are in. There is also a search bar so you can search for the character you are after, without having to scroll through all the characters to find the one you are after. Lastly you are able to filter the characters by which season they have appeared in.

Technical overview:

The app uses MVVM architecture, with koin dependency injection and modules. To display the information, Recycler views are used to scroll through the list of characters; and then when clicked on it displays even more details of the selected character in a new fragment. There is a repository that interfaces with the view model, the database- where I use the Room library- and the online server. This has been used so if there is a problem with the internet and connecting to the online server you can still view the data from the local database. Glide is used to fetch and download the images from the image url and then to display the images. Finally, there are snackbars to display whether it was able to load the data or whether there was an error(loading state).

How to run:
Run the app on an emulator as follows:
In Android Studio, create an Android Virtual Device (AVD) that the emulator can use to install and run your app.
In the toolbar, select your app from the run/debug configurations drop-down menu.
From the target device drop-down menu, select the AVD that you want to run your app on.
Target device drop-down menu.
Click Run.
If you don't have any devices configured, then you need to either connect a device via USB or create an AVD to use the Android Emulator.


